 /*

(2.0NADA NADA NADA

 */

package com.alan.facturacion;
import com.alan.sistema.facturaController;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class local extends facturaController{
protected String NombreCliente;
protected int Unidades;
protected Double Monto;


public local (String NombreCliente, int Unidades, Double Monto){
   this.NombreCliente = NombreCliente;
    this.Unidades=Unidades;
    this.Monto=Monto;


}

    public String getNombreCliente() {
        return NombreCliente;
    }

    public void setNombreCliente(String NombreCliente) {
        this.NombreCliente = NombreCliente;
    }

    public int getUnidades() {
        return Unidades;
    }

    public void setUnidades(int Unidades) {
        this.Unidades = Unidades;
    }

    public Double getMonto() {
        return Monto;
    }

    public void setMonto(Double Monto) {
        this.Monto = Monto;
    }
}
